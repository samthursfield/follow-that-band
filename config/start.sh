#!/bin/sh

set -e

# Check for the optional --debug mode.

DEBUG=no

if [ -n "$1" ]; then
    case $1 in
    --help)
        echo >&2 "Usage: $0 [--debug]"
        exit 255
        ;;
    --debug)
        DEBUG=yes
        echo "DEBUG MODE ENABLED."
        ;;
    *)
        echo >&2 "Invalid option: $1"
        echo >&2 "Run \`$0 --help\` for allowed options."
        exit 255
        ;;
    esac
fi


# First start nginx and fork

mkdir -p /run/nginx
/usr/sbin/nginx &

NGINX_PID=$!
echo "Started nginx as process $NGINX_PID"

# Set up handler to kill Nginx if subsequent steps fail.

trap "kill $NGINX_PID" TERM


if [ "$DEBUG" == "yes" ]; then
    # Run the Flask development server.

    PYTHONPATH=/srv/backend FLASK_APP=app FLASK_ENV=development \
        flask run --host 0.0.0.0 --port 5001
else
    # Run gunicorn. This may exit with an error if there is a syntax error in
    # the app code.

    /usr/bin/gunicorn \
        --access-logfile - \
        --bind 0.0.0.0:5001 \
        --pythonpath /srv/backend app:app
fi
