# Follow That Band
#
# Copyright 2018 Sam Thursfield <sam@afuera.me.uk>
#
# Follow That Band is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import authlib.flask.client
import loginpass

import logging


class TwitterError(Exception):
    def __init__(self, data={}):
        self.error = str(data.get('errors'))


def chunks(l, n):
    '''Split an interable into n-sized chunks.'''
    n = max(1, n)
    return (l[i:i+n] for i in range(0, len(l), n))


class TwitterAPI():
    '''Helper class for interacting with the Twitter API from the backend.'''

    def __init__(self, oauth, logger=None):
        self.api = loginpass.register_to(loginpass.Twitter, oauth,
                                         authlib.flask.client.RemoteApp)
        self.logger = logger or logging.getLogger(__name__)

    def find_or_create_list(self, token, list_name, list_mode='public',
                            list_description=None):
        '''Find or create a list named list_name.'''

        # First check if the list already exists. We must only call the /create
        # method if the list doesn't exist already, or we'll create a duplicate.
        #
        # A user can own a maximum of 1000 lists, we must fetch all of them
        # here.

        response = self.api.get('lists/ownerships.json',
            params={ 'count': 1000 }, token=token)

        if not response.ok:
            self.logger.error("Error from API call: {}".format(response.json()))
            raise TwitterError(response.json())

        owned_lists = response.json()

        list_id = None
        list_uri = None

        for list_info in owned_lists.get('lists', []):
            if list_info['name'] == list_name:
                list_id = list_info['id']
                list_uri = list_info['uri']
                self.logger.debug("Found list ID {} for name {}".format(list_id, list_name))

        if list_id is None:
            response = self.api.post('lists/create.json',
                params={ 'name': list_name, 'mode': list_mode,
                        'description': list_description },
                token=token)

            if not response.ok:
                self.logger.error("Error from API call: {}".format(response.json()))
                raise TwitterError(response.json())

            list_info = response.json()
            list_id = list_info['id']
            list_uri = list_info['uri']

            self.logger.debug("Created list ID {}".format(list_id))

        return list_id, list_uri

    def add_list_members(self, token, list_id, list_members):
        # Only 100 members can be added in a single call to members/create_all
        # https://developer.twitter.com/en/docs/accounts-and-users/create-manage-lists/api-reference/post-lists-members-create_all
        for chunk in chunks(list_members, 100):
            response = self.api.post('lists/members/create_all.json',
                params={ 'list_id': list_id, 'screen_name': ','.join(chunk) },
                token=token)

            if not response.ok:
                self.logger.error("Error from API call: {}".format(response.json()))
                raise TwitterError(response.json())

                response.raise_for_status()

            self.logger.info("Added {} members to list".format(len(chunk)))
