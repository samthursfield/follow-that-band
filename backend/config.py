# Follow That Band
#
# Copyright 2018 Sam Thursfield <sam@afuera.me.uk>
#
# Follow That Band is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os

try:
    SECRET_KEY = os.environ['FLASK_SECRET_KEY']

    SPOTIFY_CLIENT_ID = os.environ['SPOTIFY_CLIENT_ID']
    SPOTIFY_CLIENT_SECRET = os.environ['SPOTIFY_CLIENT_SECRET']
    SPOTIFY_CLIENT_KWARGS = { 'scope': 'user-top-read' }

    TWITTER_CLIENT_ID = os.environ['TWITTER_CLIENT_ID']
    TWITTER_CLIENT_SECRET = os.environ['TWITTER_CLIENT_SECRET']
except KeyError as e:
    raise RuntimeError("You must set {} in the environment.".format(e.args[0]))
