# Follow That Band
#
# Copyright 2018 Sam Thursfield <sam@afuera.me.uk>
#
# Follow That Band is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import authlib.flask.client
import flask
import loginpass
import requests
import werkzeug.contrib.cache

import bisect
import json
import subprocess

from exceptions import ApiClientError, ApiServerError
import reverseproxy
import twitter


app = flask.Flask(__name__)
app.config.from_pyfile('config.py')
app.wsgi_app = reverseproxy.ReverseProxied(app.wsgi_app)

# FIXME: in production we probably need a memcached or redis instance.
#
# See: https://github.com/dokku/dokku-memcached
cache = werkzeug.contrib.cache.FileSystemCache('/var/cache/www/bands-on-twitter')

oauth = authlib.flask.client.OAuth(app, cache=cache)

twitter_api = twitter.TwitterAPI(oauth, logger=app.logger)


@app.errorhandler(ApiClientError)
def api_client_error_handler(error):
    '''Return error responses for malformed AJAX requests to the frontend.'''
    return flask.jsonify({ 'error': True, 'message': str(error.message) }), 400


@app.errorhandler(ApiServerError)
def api_server_error_handler(error):
    '''Returns error responses for failed AJAX requests to the frontend.'''
    return flask.jsonify({ 'error': True, 'mesage': str(error.message) }), 500


@app.route('/')
def main():
    return 'Hello world'


def handle_spotify_authorize(remote, token, user_info):
    '''Callback for the /spotify/login endpoint.

    We store the credentials in the server side cache, and return the user ID
    and user name to the client. This page should be shown in a pop up window,
    and will call one of two functions in the main page: either
    spotify_connect_success() or spotify_connect_error().

    '''
    try:
        response = remote.get('me', token=token)

        if not response.ok:
            error = response.json()
            app.logger.error("Unable to read user info from Spotify: {}".format(str(error)))
            raise ApiServerError("Error getting user info from Spotify")

        user_info = response.json()
        spotify_user_id = user_info['id']
        spotify_user_name = user_info['display_name'] or spotify_user_id

        cache.set('spotify.' + spotify_user_id, token)

        app.logger.info("Spotify token: {}".format(token))

        return flask.render_template('spotify_auth_return.html',
                                      user_id=spotify_user_id,
                                      user_name=spotify_user_name)
    except Exception as e:
        app.logger.error("handle_spotify_authorize: Internal error: {}".format(e))
        error = "Something went wrong while connecting to Spotify"
        return flask.render_template('spotify_auth_return.html', error=error)


# Create two endpoints:
#   /spotify/login : the entry point for Spotify authentication flow
#   /spotify/auth  : the callback
app.register_blueprint(
    loginpass.create_flask_blueprint(loginpass.spotify.Spotify, oauth,
                                     handle_spotify_authorize),
    url_prefix='/spotify')


def get_closest(l, value):
    '''Returns number from 'l' that is closest to 'value'.

    The list 'l' must be sorted.

    If two numbers are equally close, return the smallest number.

    '''
    # From https://stackoverflow.com/a/12141511
    pos = bisect.bisect_left(l, value)
    if pos == 0:
        return l[0]
    if pos == len(l):
        return l[-1]
    before = l[pos - 1]
    after = l[pos]
    if after - value < value - before:
       return after
    else:
       return before


def pick_thumbnail_image(image_list, best_height=64):
    '''Pick appropriate thumbnail from image list.

    Currently this returns the image matching best_height if possible, and
    otherwise returns the first in the list at random.

    '''
    images_by_height = {image['height']:image for image in image_list}

    closest_height = get_closest(sorted(images_by_height.keys()), best_height)

    return images_by_height[closest_height]['url']


@app.route('/spotify/<user_id>/top_artists')
def spotify_top_artists_for_user(user_id):
    token = cache.get('spotify.' + user_id)

    if token is None:
        raise ApiClientError("No credentials stored for Spotify user ID {}.".format(user_id))

    try:
        cmd = ['/usr/bin/cpe', 'spotify', '--token', token['access_token'],
               '--user', user_id, 'top-artists']
        result = subprocess.run(cmd, stdout=subprocess.PIPE, check=True)
        top_artists = [json.loads(item) for item in result.stdout.decode('utf-8').strip().split('\n')]

        for artist in top_artists:
            if 'spotify.artist-image' in artist:
                image_list = artist['spotify.artist-image']
                artist['image'] = pick_thumbnail_image(image_list)
                del artist['spotify.artist-image']

        app.logger.info("Result from process: {}".format(top_artists))

        return flask.jsonify(top_artists)
    except subprocess.CalledProcessError as e:
        app.logger.error("spotify_top_artists_for_user: Internal error: {}".format(e))
        raise ApiServerError("Internal error fetching data from Spotify")


@app.route('/annotate/twitter_id')
def annotate_twitter_id():
    try:
        artist = flask.request.args['artist']
    except (KeyError, ValueError) as e:
        app.logger.info("annotate_twitter_id(): Error in parameters: {}".format(repr(e)))
        raise ApiClientError("Bad parameters: {}".format(e))

    try:
        artist_data = {'artist': artist}

        cmd = ['/usr/bin/cpe', 'musicbrainz', '--include', 'urls', '-']
        result = subprocess.run(cmd, input=json.dumps(artist_data).encode('utf-8'),
                                stdout=subprocess.PIPE, check=True)
        app.logger.info("Result from process: {}".format(result))
        annotated_artist = json.loads(result.stdout.decode('utf-8').strip())

        TWITTER_URL_PREFIX = 'https://twitter.com/'

        musicbrainz_artist_id = annotated_artist.get('musicbrainz.artist')
        artist_data['musicbrainz_artist_id'] = musicbrainz_artist_id

        for url in annotated_artist.get('musicbrainz.artist.urls', []):
            url_target = url['musicbrainz.url.target']
            if url_target.startswith(TWITTER_URL_PREFIX):
                artist_data['twitter_url'] = url_target
                artist_data['twitter_id'] = url_target[len(TWITTER_URL_PREFIX):]
                break

        return flask.jsonify(artist_data)
    except subprocess.CalledProcessError as e:
        app.logger.error("annotate_twitter_id: Internal error: {}".format(e))
        raise ApiServerError("Internal error fetching data from Musicbrainz")


def handle_twitter_authorize(remote, token, user_info):
    '''Callback for the /twitter/login endpoint.

    We store the credentials in the server side cache, and return the user ID
    and user name to the client. This page should be shown in a pop up window,
    and will call one of two functions in the main page: either
    twitter_connect_success() or twitter_connect_error().

    '''
    try:
        token = remote.authorize_access_token()

        twitter_user_id = token['user_id']
        twitter_user_name = token['screen_name']

        cache.set('twitter.' + twitter_user_id, token)

        return flask.render_template('twitter_auth_return.html',
                                     user_id=twitter_user_id,
                                     user_name=twitter_user_name)
    except Exception as e:
        app.logger.error("handle_twitter_authorize: Internal error: {}".format(e))
        error = "Something went wrong while connecting to Twitter"
        return flask.render_template('twitter_auth_return.html', error=error)


# Create two endpoints:
#   /twitter/login : the entry point for Twitter authentication flow
#   /twitter/auth  : the callback
app.register_blueprint(
    loginpass.create_flask_blueprint(loginpass.Twitter, oauth,
                                     handle_twitter_authorize),
    url_prefix='/twitter')


@app.route('/twitter/list/<list_name>', methods=['POST'])
def twitter_list(list_name):
    '''Create a Twitter list.'''
    params = flask.request.get_json()

    try:
        twitter_user_id = params['user_id']
        list_members = params['list_members']
    except (KeyError, ValueError) as e:
        app.logger.info("twitter_list(): Error in parameters: {}".format(repr(e)))
        raise ApiClientError("Bad parameters: {}".format(e))

    if len(list_members) == 0:
        raise ApiClientError("Refusing to create a list with 0 members.")

    if len(list_members) > 5000:
        # This limit comes from Twitter:
        # https://developer.twitter.com/en/docs/accounts-and-users/create-manage-lists/api-reference/post-lists-members-create_all
        raise ApiClientError("Unable to create a list with > 5000 members.")

    token = cache.get('twitter.' + str(twitter_user_id))

    if token is None:
        raise ApiClientError("No credentials stored for user ID {}.".format(twitter_user_id))

    try:
        list_id, list_uri = twitter_api.find_or_create_list(
            token, list_name,
            list_description="The best musicians in the world to follow. Created by Bands On Twitter.")

        twitter_api.add_list_members(token, list_id, list_members)

        return flask.jsonify(success=True, list_id=list_id, list_uri=list_uri)
    except (KeyError, ValueError, twitter.TwitterError, requests.exceptions.HTTPError) as e:
        app.logger.error("twitter_list(): {}".format(repr(e)))
        raise ApiServerError(str(e))

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
