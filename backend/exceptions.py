# Follow That Band
#
# Copyright 2018 Sam Thursfield <sam@afuera.me.uk>
#
# Follow That Band is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


class ApiClientError(Exception):
    def __init__(self, message):
        self.message = message


class ApiServerError(Exception):
    def __init__(self, message):
        self.message = message
