# Follow That Band

This is the source code for the Follow That Band webapp. The app makes it
easier to follow your favourite musicians on [Twitter](https://twitter.com/),
by automatically following the artists you listen to most on
[Spotify](https://www.spotify.com).

The home of the app is: <http://followthatband.afuera.me.uk/>

This source code is distributed under the terms of the [GNU Affero General Public License](https://www.gnu.org/licenses/agpl-3.0.en.html) (version 3 or newer).

## Deployment

This app is intended to be deployed to a
[Dokku](http://dokku.viewdocs.io/dokku/) instance.

## Local testing

You can build and run the application locally using Docker, as shown
below. If you prefer to use [Podman](podman.io) in place of Docker, just
substitute `docker` with `podman` in the commands.

    docker build --tag . followthatband
    docker run --env-file ./dummy.secrets -i -t --rm -p 5000:5000 followthatband

The app will now be available at http://localhost:5000/

Integration with 3rd party APIs will not work unless you provide API
credentials. Visit the 'developer' section of the 3rd party websites in order
to obtain credentials.

## Developer conveniences

During development you can run the app in debug mode.

    docker run --env-file ./dummy.secrets -i -t --rm -p 5000:5000 followthatband /root/start.sh --debug

This will the app to run with the Flask development server instead of with
Gunicorn. Debug logs from the application will appear on the console in this
mode, and the Flask debugger will appear if any application errors occur.

You can optionally mount your working tree of the application side the
container. In debug mode, Flask will monitor for changes to the code and will
reload the app automatically if any are detected. You can run the app with the
source tree mounted with a command like the following:

    docker run  --env-file ./dummy.secrets -i -t --rm -p5000:5000 --mount type=bind,source=$(pwd),destination=/srv followthatband  /root/start.sh --debug
