FROM alpine:3.9.2

RUN apk upgrade
RUN apk add nginx python3 py3-gunicorn

RUN pip3 install flask


# All deps of authlib & loginpass
#
# We use a bundled copy of loginpass as the version on PyPI (v0.1) is too old.
RUN apk add py3-cffi py3-cryptography
RUN pip3 install authlib
COPY ./subprojects/loginpass /root/subprojects/loginpass
RUN pip3 install /root/subprojects/loginpass

# Calliope
COPY ./subprojects/calliope /root/subprojects/calliope
RUN pip3 install musicbrainzngs parsedatetime pyxdg spotipy
RUN apk add meson && \
    cd /root/subprojects/calliope && \
    mkdir __build && \
    cd __build && \
    meson .. --prefix=/usr -Dauto_features=disabled -Ddocs=false -Dmusicbrainz=enabled -Dspotify=enabled -Dtestsuite=false && \
    ninja install && \
    apk del meson ninja

COPY ./backend /srv/backend
COPY ./site /srv/site
COPY config/start.sh /root
COPY config/nginx.conf /etc/nginx/nginx.conf

CMD /root/start.sh

EXPOSE 5000
